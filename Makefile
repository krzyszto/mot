SRC= mot.cpp
BIN=mot
CXXFLAGS= -std=c++11 -O0 -g -Wall -Wextra
CXX=g++
all: $(BIN)

$(BIN): $(SRC)
	$(CXX) $(CXXFLAGS) $(SRC) -o $(BIN)
clean:
	rm $(BIN)
