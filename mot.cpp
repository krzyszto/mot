#include <stdio.h>
#include <algorithm>
#include <set>
#include <vector>
#include <limits.h>
#include <unordered_set>
#include <tuple>

struct kolejka_piorytetowa {
	std::set<std::pair<unsigned int, unsigned int> > kolejka;
	void push(std::pair<unsigned int, unsigned int> &para);
	std::pair<unsigned int, unsigned int> top();
	bool is_empty();
	bool pop();
	void clear();
};

const unsigned int KOSZT_WYJSCIA_Z_DOMU = 0;
const unsigned int POZYCJA_DOMU = 0;
const unsigned int NIE_BYLISMY = UINT_MAX;

void kolejka_piorytetowa::push(std::pair<unsigned int, unsigned int> &para)
{
	kolejka.insert(para);
}

void kolejka_piorytetowa::clear()
{
	kolejka.clear();
}

bool kolejka_piorytetowa::is_empty()
{
	return kolejka.empty();
}

bool kolejka_piorytetowa::pop()
{
	if (this->is_empty())
		return false;
	kolejka.erase(kolejka.begin());
	return true;
}

std::pair<unsigned int, unsigned int> kolejka_piorytetowa::top()
{
	return *(kolejka.begin());
}

int wyznacz_min(const unsigned int *const droga_do, const unsigned int *const droga_z,
		const unsigned int ile_przystankow)
{
	unsigned int wynik = INT_MAX;
	for (unsigned int i = 0; i < ile_przystankow; i++)
		if ((droga_z[i] < wynik) && (droga_do[i] < wynik))
			wynik = std::max(droga_z[i], droga_do[i]);
	return wynik;
}

// Sprawdza, czy dane pary pokazuja ten sam wierzcholek.
bool ten_sam_wierzcholek(const std::pair<unsigned int, unsigned int> &para1,
			 const std::pair<unsigned int, unsigned int> &para2)
{
	return para1.second == para2.second;
}

// Sprawdza, czy zbior wierzcholkow, do ktorych doszlismy
// zawiera juz dany wierzcholek.
bool doszlismy(const unsigned int *const doszlismy, const int pozycja)
{
	return (doszlismy[pozycja] != NIE_BYLISMY);
}

void wrzuc_z_wierzcholka(const unsigned int numer_wierzcholka,
			 const std::vector<std::pair<unsigned int, unsigned int> >
			 *const krawedzie,
			 kolejka_piorytetowa &kolejka,
			 const unsigned int *const bylismy)
{
	for (std::pair<unsigned int, unsigned int> krawedz :
		     krawedzie[numer_wierzcholka]) {
		if (!doszlismy(bylismy, krawedz.first)) {
			unsigned int tmp = bylismy[numer_wierzcholka] + krawedz.second;
			std::pair<unsigned int, unsigned int> tmp_para =
				std::make_pair(tmp, krawedz.first);
			kolejka.push(tmp_para);
		}
	}
}

void dijkstra(const std::vector<std::pair<unsigned int, unsigned int> >
	      *const krawedzie,
	      unsigned int *const doszlismy, const unsigned int start)
{
	kolejka_piorytetowa radix;
	std::pair <unsigned int, unsigned int> tmp_pair =
		std::make_pair(KOSZT_WYJSCIA_Z_DOMU, start);
	radix.push(tmp_pair);
	while (!radix.is_empty()) {
		std::pair<int, int> tmp;
		tmp = radix.top();
		radix.pop();
		if (doszlismy[tmp.second] == NIE_BYLISMY) {
			doszlismy[tmp.second] = tmp.first;
			wrzuc_z_wierzcholka(tmp.second, krawedzie, radix,
					    doszlismy);
		}
	}
}

int main()
{
	unsigned int *droga_do;
	unsigned int *droga_z;
	unsigned int liczba_postojow, liczba_krawedzi;
	kolejka_piorytetowa k;
	scanf("%u%u", &liczba_postojow, &liczba_krawedzi);
	std::vector<std::pair<unsigned int, unsigned int> > *lista_sasiedztwa =
		new std::vector<std::pair<unsigned int, unsigned int> >
		[liczba_postojow];
	std::tuple<unsigned int, unsigned int, unsigned int> *wejscie =
		new std::tuple<unsigned int, unsigned int, unsigned int>
		[liczba_krawedzi];
	for (unsigned int i = 0; i < liczba_krawedzi; i++) {
		unsigned int a, b, c;
		scanf("%u%u%u", &a, &b, &c);
		wejscie[i] = std::make_tuple(a, b, c);
	}
	for (unsigned int i = 0; i < liczba_krawedzi; i++) {
		unsigned int pocz, kon, waga;
		pocz = std::get<0>(wejscie[i]);
		kon = std::get<1>(wejscie[i]);
		waga = std::get<2>(wejscie[i]);
		lista_sasiedztwa[pocz].push_back(std::make_pair(kon, waga));
	}
	droga_do = new unsigned int[liczba_postojow];
	droga_z = new unsigned int[liczba_postojow];
	for (unsigned int i = 0; i < liczba_postojow; i++) {
		droga_do[i] = NIE_BYLISMY;
		droga_z[i] = NIE_BYLISMY;
	}
	dijkstra(lista_sasiedztwa, droga_do, 0);
	delete[] lista_sasiedztwa;
	lista_sasiedztwa =
		new std::vector<std::pair<unsigned int, unsigned int> >
		[liczba_postojow];
	for (unsigned int i = 0; i < liczba_krawedzi; i++) {
		unsigned int pocz, kon, waga;
		pocz = std::get<1>(wejscie[i]);
		kon = std::get<0>(wejscie[i]);
		waga = std::get<2>(wejscie[i]);
		lista_sasiedztwa[pocz].push_back(std::make_pair(kon, waga));
	}
	delete[] wejscie;
	dijkstra(lista_sasiedztwa, droga_z, liczba_postojow - 1);
	printf("%u\n", wyznacz_min(droga_do, droga_z, liczba_postojow));
	return 0;
}
